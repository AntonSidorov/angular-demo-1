import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'Demo 1';
  reminders = [
    new Reminder('English Exam', new Date('Nov 1 2017 09:30:00 GMT+1100 (AEST)')),
    new Reminder('Specialist Exam 1', new Date('Nov 10 2017 09:30:00 GMT+1100 (AEST)')),
    new Reminder('Specialist Exam 2', new Date('Nov 13 2017 15:00:00 GMT+1100 (AEST)')),
    new Reminder('Chemistry Exam', new Date('Nov 14 2017 09:00:00 GMT+1100 (AEST)')),
    new Reminder('Software Development', new Date('Nov 16 2017 15:00:00 GMT+1100 (AEST)'))
  ];
  ngOnInit(){
  }
}

export class Reminder {
  name: string;
  date: Date;
  constructor(name, date) {
    this.name = name;
    this.date = date;
  }

  // d1 is the date right now.
  public timeTillReminder(d1: Date) {
    let totalSec = Math.round(+this.date - +d1) / 1000;
    let seconds = totalSec;
    let days = Math.floor(seconds / (60 * 60 * 24)); seconds -= days * (60 * 60 * 24);
    let hours = Math.floor(seconds / (60 * 60)); seconds -= hours * (60 * 60);
    let minutes = Math.floor(seconds / 60); seconds -= minutes * 60;
    seconds = Math.floor(seconds);

    if (days < 4) {
      days = 0; hours += days * 24;
      if (hours < 11) {
        hours = 0; minutes += hours * 60
        if (minutes < 17) { minutes = 0; seconds += minutes * 60 }
      }
    }

    return `${days > 0 ? `${days}d` : ''} ${hours > 0 ? `${hours}h` : ''} ${minutes > 0 ? `${minutes}m` : ''} ${seconds}s`;
  }
}
