import { Component, OnInit, Input } from '@angular/core';

import { Reminder } from '../app.component';

@Component({
  selector: 'app-reminder',
  templateUrl: './reminder.component.html',
  styleUrls: ['./reminder.component.scss']
})
export class ReminderComponent implements OnInit {

  time: Date = new Date();
  @Input() reminder: Reminder;
  constructor() { }

  ngOnInit() {
    console.log(this.reminder);
    setInterval(() => {
      this.time = new Date();
    });
  }

}
